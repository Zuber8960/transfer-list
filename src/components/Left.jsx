import React, { Fragment } from "react";

const Left = ({ data , setNewData}) => {


    function clickOnTag(event){
        // console.log(event.target.id === data[0].id);
        const result = data.reduce((arr, {...obj}) => {
            if(event.target.id == obj.id){
                obj.isSelected = obj.isSelected ? false : true;
            };
            arr.push(obj);
            return arr;
        },[]);
        console.log(result);

        setNewData(result);
    }

  return (
    <div className="left">
      {data.filter(obj=> {
            return obj.isLeft;
        })
        .map((object) => {
            // console.log(object.id);
          return <div key={object.id}>
            <input type="checkbox" id={object.id} onClick={clickOnTag}/>
            <label htmlFor={object.id}>{object.name}</label>
          </div>;
        })}
    </div>
  );
};

export default Left;
