import React from "react";

const Middle = ({ newArr, setNewArr }) => {
  function transferAllTOLeft() {
    const result = newArr.map(({ ...object }) => {
      object.isLeft = true;
      return object;
    });
    setNewArr(result);
    return result;
  }

  function transferAlltoRight() {
    const result = newArr.map(({ ...obj }) => {
      obj.isLeft = false;
      return obj;
    });
    setNewArr(result);
    return result;
  }

  function sendSomeToLeft() {
    const result = newArr.map(({ ...obj }) => {
      if (obj.isSelected && obj.isLeft === false) {
        obj.isLeft = true;
        obj.isSelected = false;
      }
      return obj;
    });
    // console.log(result);
    setNewArr(result);

    return result;
  }

  function sendSomeToRight() {
    const result = newArr.map(({ ...obj }) => {
      if (obj.isSelected && obj.isLeft === true) {
        obj.isLeft = false;
        obj.isSelected = false;
      }
      return obj;
    });
    console.log(result);
    setNewArr(result);

    return result;
  }

  return (
    <div className="middle">
      <button onClick={transferAllTOLeft}>
        <i className="fa-solid fa-angles-left fa-2xl"></i>
      </button>
      <button onClick={sendSomeToLeft}>
        <i className="fa-solid fa-angle-left fa-2xl"></i>
      </button>
      <button>
        <i
          onClick={sendSomeToRight}
          className="fa-solid fa-angle-right fa-2xl"
        ></i>
      </button>
      <button onClick={transferAlltoRight}>
        <i className="fa-solid fa-angles-right fa-2xl"></i>
      </button>
    </div>
  );
};

export default Middle;
