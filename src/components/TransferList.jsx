import React, { useState } from "react";
import Left from "./Left";
import Middle from "./Middle";
import Right from "./Right";

const TransferList = () => {
    const arr = [
        {   
            id:1,
            name: "Svelte",
            isSelected: false,
            isLeft: true
        },
        {
            id:2,
            name: "HTML",
            isSelected: false,
            isLeft: true
        },
        {
            id:3,
            name: "JS",
            isSelected: false,
            isLeft: true
        },
        {
            id:4,
            name: "React",
            isSelected: false,
            isLeft: true
        },
        {   
            id:5,
            name: "TS",
            isSelected: false,
            isLeft: true
        },
        {
            id:6,
            name: "CSS",
            isSelected: false,
            isLeft: true
        },
        {   
            id:7,
            name: "Angular",
            isSelected: false,
            isLeft: false
        },
        {   
            id:8,
            name: "Vue",
            isSelected: false,
            isLeft: false
        }
    ]

    const [newArr, setNewArr] = useState(arr);
    // console.log(newArr);

  return (
    <>
        <header>
            <p>Home</p>
            <p>Transfer List</p>
            <i className="fa-brands fa-github"></i>
        </header>
        <div className="container">
            <Left data={newArr} setNewData={setNewArr}/>
            <Middle newArr={newArr} setNewArr={setNewArr}/>
            <Right data={newArr} setNewData={setNewArr}/>
        </div>
    </>
  )
};

export default TransferList;
