import React from "react";

const Right = ({data, setNewData}) => {
    function clickOnTag(event){
        const result = data.reduce((arr, {...obj}) => {
            if(event.target.id == obj.id){
                obj.isSelected = obj.isSelected ? false : true;
            };
            arr.push(obj);
            return arr;
        },[]);
        // console.log(result);

        setNewData(result);
    }

  return (
    <div className="right">
        {data.filter(object => {
            return object.isLeft===false;
        })
        .map(object => {
            // console.log(object.id);
            return <div key={object.id}>
            <input type="checkbox" id={object.id} onClick={clickOnTag}/>
            <label htmlFor={object.namide}>{object.name}</label>
          </div>
        })}
      
    </div>
  );
};

export default Right;
