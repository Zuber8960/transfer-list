import React from "react"
import TransferList from "./components/TransferList"

const App = () => {
  return (
    <>
      <TransferList />
    </>
  );
};

export default App
